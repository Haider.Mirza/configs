(setq user-full-name "Haider Mirza"
      user-mail-address "thehumanrider@gmail.com")

(setq doom-theme 'doom-one)

(setq org-directory "~/org/")

(setq display-line-numbers-type t)
